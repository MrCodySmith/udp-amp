#!/usr/bin/env python3

import argparse
from scapy.all import *
import subprocess
import os
import operator

# UDP Amplificaiton Example:
# While slides and videos can often easliy show what UDP amplification looks like,
# this python3 script shows the size and power of amplifcation vectors

## TODO: Format the output
## TODO: Auto-grab interface and source ip ( precheck? )
## TODO: Implement LDAP Amplification
## TODO: Implement NTP Amplification
## TODO: MS-SQL Amp?


def dns(destination_resolver, zone):

    # init
    results = {}
    query_number = 0

    # local data:
    # Note: Bring your own data here
    iface = ""
    source_ip = ""

    ttl = 128
    query_name = zone
    query_type = ["ANY", "A","MX","NS","TXT", "SOA"] # DNS Query Record types

    # Loop through all query types then all DNS servers
    for i in range(0,len(query_type)):
        query_number += 1

        # create dns packet
        packet = IP(src=source_ip, dst=destination_resolver, ttl=ttl) / UDP() / DNS(rd=1, qd=DNSQR(qname=query_name, qtype=query_type[i]))

        # fire
        try:
            query = sr1(packet,iface,verbose=False, timeout=10)
        except Exception as ex:
            print(ex)
            pass

        try:
            qtype = query_type[i]
            results[qtype] = {}
            query_size = len(packet)
            result_size = len(query)
            amp_factor = len(query) / len(packet)
            results[qtype]['Resolver'] = destination_resolver
            results[qtype]['query_size'] = query_size
            results[qtype]['response_size'] = result_size
            results[qtype]['amp_factor'] = amp_factor

        except Exception as ex:
            print(ex)
            pass

    return(results)


def report(results, domain, resolver):

    amp_report = ''
    linebreak = '================================================================================\n'
    header = linebreak + f'Domain: {domain} || Resolver: {resolver}\n' + linebreak + f'{"QTYPE":<6} {"Q-Size":<12} {"R-Size":<12}  {"AMP":<12}\n'

    for x in results:
        amp_report = amp_report + f'{x:<6} {results[x]["query_size"]:<12} {results[x]["response_size"]:<12} {results[x]["amp_factor"]:<12}\n'

    print(header)
    print(amp_report)


def main():

    argparser = argparse.ArgumentParser()
    argparser.add_argument('--resolver', help='DNS Resolver to point our queries at.')
    argparser.add_argument('--zone', help='The zone to get query data for.')
    argparser.add_argument('--dns', help='Get DNS reflection data.', action='store_true')
    args = argparser.parse_args()

    if args.resolver:
        resolver = args.resolver
        print(f'Using the resolver: {resolver}')

    elif not args.resolver:
        resolver = '8.8.8.8'

    if args.zone:
        zone = args.zone

    elif not args.zone:
        zone = 'google.com'

    if args.dns:
        dns_results = dns(resolver, zone)

        if dns_results:
            report(dns_results, zone, resolver)


if __name__ == '__main__':
    main()
