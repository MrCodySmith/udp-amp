# UDP Amplification Example

Often during security talks, I get the question, "Well how useful is $PROTOCOL Amplifcation?"
While it would be easy to just spout of numbers and statistics, sometimes, the most powerful
tool to show how powerful something is, is a tool itself. 

# Requirements: 

- FreeBSD
- Python3
- Scapy

# To Do: 

- Add LDAP Amplifcation Vectors
- Add MS-SQL Amplifcation Vectors
- Add NTP Amplifcation Vectors
